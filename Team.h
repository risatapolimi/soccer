#include "Player.h"
#include <stdlib>

using namespace std;


class Team {
	char id; 
	Player* players;
	Player* substituted_players;
	int i;
	int N; //numero massimo di giocatori

public:
	Team(char id_i, int n){
		id = id_i;
		players = (Player*) malloc(sizeof(Player)*n); 
		N = n;
	}

	void addPlayer(Player p){
		if(i<N){
			players[i++] = p;
		}
	}
};