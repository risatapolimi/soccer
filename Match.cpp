//#include <mpi.h>

#include <iostream>

#include "Team.h"
#include "Sensor.h"
#include "SensorPlayer.h"
#include "Player.h"

using namespace std;

class Match {
	Team team_a, team_b;
	int[30] balls;
	int ball_active; //indice della palla attiva
	Sensor[] sensors;

	double time_start, time_end; //tempi di pausa ancora da considerare

public:
	Match (Team a_i, Team b_i, Ball[] balls_i, Sensor[] sensors_i){
		a = a_i;
		b = b_i;
		balls = balls_i;
		ball_active = 0;
		sensors = sensors_i;
	}
};

int main(int argc, char **argv){
	int k, t;

	//lettura dalla linea di comando oppure si può fare da un file di configurazione
	if(argc != 3){
		cout << "Arguments number does not match. Please insert (only) K and T values.";
		return 1;
	}

	//togliere stampa
	 printf("argc=%d\n",argc);
	 for(i=0;i < argc;++i){
	   printf("argv[%d]:=%s\n",i,argv[i]);
	 }
	 //////

	 k = argv[1];
	 if(k<1 || k>5){
	 	cout << "Out of range K value. Please use values between 1 and 5.";
	 	return 2;
	 }

	 t = argv[2];
	 if(k<1 || k>60){
	 	cout << "Out of range T value. Please use values between 1 and 60.";
	 	return 3;
	 }

	 //Inizia il codice di elaborazione da qui

	 FILE *fp_meta;

	 if((fp_meta = fopen("metadata.txt", "r")) == NULL){
	 	cout << "Metadata file not found";
	 	return 4;
	 }

	 char row[80];
	 while(!feof(fp_meta)){
	 	fgets(row, fp_meta);
	 											cout << row;
	 	if(strcmp(row, "Balls:") == 0){
	 		while (row != "\0"){
	 			char* balls_list;
	 			fgets(row, fp_meta);
	 			balls_list = strtok(row, ":");
	 			int i = 0;
	 			while(balls_list != NULL){
	 				balls_list = strtok(NULL, ",");
	 											cout << balls_list;
	 				balls[i++] = atoi(balls_list);
	 			}
	 		}

	 	}
	 	else if(strcmp(row, "Team A") == 0){
	 		fgets(row, fp_meta);
	 		fgets(row, fp_meta);
	 		fgets(row, fp_meta);

			char* name;

			team_a = Team('A', 8);

			Player player;
			int j = 0;
	 		while (row != "\0"){

	 			token = strtok (row, "(");
	 			name = token;
	 			player = Player(name);
	 			team_a.addPlayer(player);

	 			char * token_sensor;
	 			while (token != NULL){
	 				token = strtok(NULL, ",");
	 				token_sensor = atoi(strtok(token, ":"));
	 				sensor[s++] = SensorPlayer(token_sensor, player);
	 			}
	 		}
	 	}
	 		 	else if(strcmp(row, "Team B") == 0){
	 		fgets(row, fp_meta);
	 		fgets(row, fp_meta);
	 		fgets(row, fp_meta);

			char* name;

			team_b = Team('B', 8);

			Player player;
			int j = 0;
	 		while (row != "\0"){

	 			token = strtok (row, "(");
	 			name = token;
	 			player = Player(name);
	 			team_b.addPlayer(player);

	 			char * token_sensor;
	 			while (token != NULL){
	 				token = strtok(NULL, ",");
	 				token_sensor = atoi(strtok(token, ":"));
	 				sensor[s++] = SensorPlayer(token_sensor, player);
	 			}
	 		}
	 	}
	 }

	return 0;
}

