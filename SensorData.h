using namespace std;

class SensorData {
	int id, x, y, z; //id of the sensor, x-y-z coordinates of the sensor 
	double timestamp; //event timestamp in picoseconds

public:
	SensorData(int id_i, int x_i, int y_i, int z_i, double timestamp_i){
		id = id_i;
		x = x_i;
		y = y_i;
		z = z_i;
		timestamp = timestamp_i;
	}
};

