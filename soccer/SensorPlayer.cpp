//
// Created by Daniele on 13/08/2018.
//

#include "SensorPlayer.h"

SensorPlayer::SensorPlayer() : Sensor(){}

SensorPlayer::SensorPlayer(int id_i,Player * player_i):Sensor(id_i){
    player=player_i;
}

Player * SensorPlayer::getPlayer(){
    return player;
}

void SensorPlayer::addFractionElement(double fraction){
    fraction_possession_time.push_back(fraction);
}

vector <double> * SensorPlayer::getFractionVector(){
    return &fraction_possession_time;
}

void SensorPlayer::addFractionVector(vector<double> * fractions) {
    fraction_possession_time.insert(fraction_possession_time.end(), fractions->begin(), fractions->end());
}

void SensorPlayer::printState() {
    Sensor::printState();
    player->printState();
}

double SensorPlayer::collectFractions() {
    double sum = 0;
#pragma omp parallel for reduction(+: sum)
    for (int i = 0; i < getFractionVector()->size(); i++) {
        sum += getFractionVector()->at(i);
    }
 //   printf("Possesso aggiunto [%s][%d]: %f\n", getPlayer()->getName().c_str(),id, sum);
    return sum;
}
