//
// Created by Daniele on 14/08/2018.
//

#include "Data.h"

int Data::getId(){
    return id;
}

double Data::getTimestamp(){
    return timestamp;
}

int Data::getX(){
    return x;
}

int Data::getY(){
    return y;
}

int Data::getZ(){
    return z;
}

Data::Data(int id_i, double ts, int x_i, int y_i, int z_i){
    id=id_i;
    timestamp=ts;
    x=x_i;
    y=y_i;
    z=z_i;
}

double Data::distance(Data data) {
    return sqrt(pow(x-data.getX(),2)+pow(y-data.getY(),2)+pow(z-data.getZ(),2));
}

void Data::printState() {
    cout << "Data: "<< id << "; " << timestamp << "; " << " (" << x << ", " << y << ", " << z << ")" << endl;
}

Data::Data(){}

bool Data::operator==(const Data &rhs) const {
    return id == rhs.id &&
           timestamp == rhs.timestamp;
}

bool Data::operator!=(const Data &rhs) const {
    return !(rhs == *this);
}

bool Data::isReferee() {
    return id==105 || id==106;
}

void Data::setId(int id) {
    Data::id = id;
}

void Data::setTimestamp(double timestamp) {
    Data::timestamp = timestamp;
}
/*
Data::Data(const Data &data) {
    id=data.id;
    timestamp=data.timestamp;
    x= data.x;
    y=data.y;
    z=data.z;
}
*/