//
// Created by Marco Sartini on 17/08/2018.
//

#include <iostream>
#include "Interruption.h"

double Interruption::getTimestamp(){
    return timestamp;
}

TypeOfEvent Interruption::getType(){
    return type;
}

Interruption::Interruption(double timestamp, TypeOfEvent type) : timestamp(timestamp), type(type) {}

void Interruption::printState() {
    cout << "Interruption: " << type << " at " << timestamp << endl;
}
