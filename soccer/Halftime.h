//
// Created by Daniele on 14/08/2018.
//
#include <vector>

using namespace std;

#ifndef SOCCER_HALFTIME_H
#define SOCCER_HALFTIME_H


#include "Sensor.h"

class Halftime {
public:
    Halftime();
    Halftime(string n,double s, double e, vector<Sensor> balls_i);
    string getIDName();
    Sensor getActiveBall();
    void setActiveBall(Sensor sensor);
    bool hasBall(int id_i);
    void printState();
private:
    string id_name;
    double start_time;
    double end_time;
    vector<Sensor> balls;
    Sensor active_ball;

};


#endif //SOCCER_HALFTIME_H
