//
// Created by Daniele on 14/08/2018.
//

#include "Halftime.h"

Halftime::Halftime() {
    id_name="";
    start_time=0;
    end_time=0;
}

Halftime::Halftime(string n,double s, double e, vector<Sensor> balls_i){
    id_name=n;
    start_time=s;
    end_time=e;
    balls=balls_i;
    active_ball=balls_i.at(0);
}

string Halftime::getIDName(){
    return id_name;
}

Sensor Halftime::getActiveBall(){
    return active_ball;
}
void Halftime::setActiveBall(Sensor sensor){
    active_ball=sensor;
}

void Halftime::printState() {
    cout << id_name << " start time: " << start_time << " end time: " << end_time << " ball_used: ";
    for(int i=0;i<balls.size();i++){
        cout << balls.at(i).getId() << "; ";
    }
    cout << endl;
}

bool Halftime::hasBall(int id_i) {
    for(int i=0;i<balls.size();i++){
        if(id_i==balls.at(i).getId())
            return true;
    }
    return false;
}