//
// Created by Daniele on 13/08/2018.
//

#include <string>
#include "Team.h"

Team::Team(){
    name="";
    team_possesion=0;
}

Team::Team(string name_i){
    name=name_i;
    team_possesion=0;
}

Team::Team(string name_i,vector<Player*> pl_i){
    name=name_i;
    team_possesion=0;
    players=pl_i;
}

string Team::getName(){
    return name;
}

vector<Player*> Team::getPlayers(){
    return players;
}

void Team::setPlayers(vector<Player*> pl_i){
    players=pl_i;
}

Player * Team::getPlayer(string name_g) {
    for(int i=0;i<players.size();i++){
        if(players.at(i)->getName()==name_g)
            return players.at(i);
    }
    throw std::invalid_argument("Player not found");
}

double Team::getTeamPossession(){
    return team_possesion;
}

void Team::addTeamPossession(int inc){
    team_possesion+=inc;
}


void Team::printState() {
    cout << "TEAM " << name << endl;
    for(int i=0;i<players.size();i++){
        cout << (i+1) << ". ";
        players.at(i)->printState();
    }

    cout << "Team Possesion: " << team_possesion << endl;
}

/***
 * Somma il possesso di tutti i giocatori. Non si occupa degli halftime quindi quella cosa va gestita in qualche modo
 */
void Team::collectPossession() {
    team_possesion = 0;
    Player * p;
//#pragma omp parallel for
    for (int i = 0; i < players.size(); i++) {
        team_possesion += players.at(i)->getPossession();
    }
}
