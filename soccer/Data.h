//
// Created by Daniele on 14/08/2018.
//
#include <math.h>
#include <iostream>
#include <string>

using namespace std;

#ifndef SOCCER_DATA_H
#define SOCCER_DATA_H


class Data {
public:
    Data();
//    Data (const Data &data);
    Data(int id_i,double ts,int x_i,int y_i,int z_i);
    int getId();
    double getTimestamp();
    int getX();
    int getY();
    int getZ();
    double distance(Data data);

    void setId(int id);

    void setTimestamp(double timestamp);

    void printState();

    bool operator==(const Data &rhs) const;

    bool operator!=(const Data &rhs) const;

    bool isReferee();

private:
    int id;
    double timestamp;
    int x,y,z;
};


#endif //SOCCER_DATA_H
