//
// Created by Daniele on 13/08/2018.
//
#include <string>
#include <iostream>

using namespace std;

#ifndef SOCCER_SENSOR_H
#define SOCCER_SENSOR_H


class Sensor {
public:
    Sensor();
    Sensor(int id_i);
    int getId();
    void printState();
protected:
    int id;
};


#endif //SOCCER_SENSOR_H
