//
// Created by Daniele on 13/08/2018.
//

#include <iostream>
#include <string>
#include <vector>
#include "Player.h"

using namespace std;

#ifndef SOCCER_TEAM_H
#define SOCCER_TEAM_H


class Team {

public:
    Team();
    Team(string name_i);
    Team(string name_i,vector<Player*> pl_i);
    string getName();
    vector<Player*> getPlayers();
    void setPlayers(vector<Player*> pl_i);
    Player * getPlayer(string name_g);
    double getTeamPossession();
    void addTeamPossession(int inc);
    void collectPossession(); //somma il possesso di tutti i giocatori
    void printState();
private:
    string name;
    vector<Player*> players;
    double team_possesion;
};


#endif //SOCCER_TEAM_H
