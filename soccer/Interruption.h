//
// Created by Marco Sartini on 17/08/2018.
//

using namespace std;

#ifndef SOCCER_INTERRUPTION_H
#define SOCCER_INTERRUPTION_H



enum TypeOfEvent {interr_begin, interr_end};
class Interruption {

public:
    Interruption();
    Interruption(double timestamp, TypeOfEvent type);
    double getTimestamp();
    TypeOfEvent getType();
    void printState();

private:
    double timestamp;
    TypeOfEvent type;

};


#endif //SOCCER_INTERRUPTION_H
