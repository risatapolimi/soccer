//
// Created by tavec on 13/08/2018.
//

#include <iostream>
#include <string>

using namespace std;

#ifndef SOCCER_PLAYER_H
#define SOCCER_PLAYER_H

class Player {
public:
    Player();
    Player(string name_i);
    double getPossession();
    void addPossession(double i);
    string getName();
    void printState();
private:
    std::string name;
    double possession;
};

#endif //SOCCER_PLAYER_H
