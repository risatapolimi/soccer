//
// Created by tavec on 13/08/2018.
//

#include "Player.h"

Player::Player(){
    name="";
    possession=0;
}

Player::Player(string name_i) {
    name = name_i;
    possession = 0;
}

double Player::getPossession(){
    return possession;
}

void Player::addPossession(double i){
    possession +=i;
}

string Player::getName(){
    return name;
}

void Player::printState() {
    cout << "Player: " << name << "\t\tPossession: " << possession << endl;
}

