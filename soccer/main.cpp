#include <omp.h>
#include <iostream>
#include <sstream>
#include <map>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <vector>
#include "Player.h"
#include "Team.h"
#include "Sensor.h"
#include "SensorPlayer.h"
#include "Halftime.h"
#include "Data.h"
#include "SoccerField.h"
#include "Interruption.h"
#include "csv.h"

using namespace std;
const int CONVERSION = 2000; //1000

vector<Player*> teamAnalysis(FILE* fp_meta,vector<SensorPlayer> * sensor_players, map<int, SensorPlayer> * sensor_players_map);
void readGameInterruptions(vector<Interruption> * interruptions);
bool isDataGameInterrupted(Data * d, int * index_interruption, vector<Interruption> * interruptions);
Data nearestData(map<int, Data> map, Data ball,double min,Data old_nearest);

int main(int argc, char **argv) {
    int k, t, minimum_distance_mm;
    Team team_a;
    Team team_b;
    vector<Sensor> balls;
    vector<SensorPlayer> sensor_players;
    map<int, Data> positions;
    map<int, SensorPlayer> sensor_players_map;
    map<int, SensorPlayer> sensor_players_map_private;
    vector<Halftime> halftimes;
    //0,33965), (0,-33960),(52483,33965),(52483,-33960)
    SoccerField field(0, 52483, 33965, -33960);


    //lettura dalla linea di comando oppure si può fare da un file di configurazione
    if (argc != 3) {
        cout << "Arguments number does not match. Please insert (only) K and T values.";
        return 1;
    }

    //togliere stampa

    printf("argc=%d\n", argc);
    for (int i = 0; i < argc; i++) {
        printf("argv[%d]:=%s\n", i, argv[i]);
    }


    k = atoi(argv[1]);

    if (k < 1 || k > 5) {
        cout << "Out of range K value. Please use values between 1 and 5.";
        return 2;
    }

    t = atoi(argv[2]);
    if (t < 1 || t > 60) {
        cout << "Out of range T value. Please use values between 1 and 60.";
        return 3;
    }

    minimum_distance_mm = k * CONVERSION;

    //Inizia il codice di elaborazione da qui

    FILE *fp_meta;

    if ((fp_meta = fopen("res/metadata.txt", "r")) == NULL) {
        cout << "Metadata file not found";
        return 4;
    }
    //ifstream in("metadata.txt");

    char row[80];

    fgets(row, 79, fp_meta);
    while (!feof(fp_meta)) {

        if (strncmp(row, "Balls:", 6) == 0) {
            fgets(row, 79, fp_meta);
            while (strncmp(row, "\r", 1) != 0) {
                char *balls_list, *name_half;
                balls_list = strtok(row, ":");
                name_half = balls_list;

                while (balls_list != NULL) {
                    balls_list = strtok(NULL, ", \n");
                    if (balls_list != NULL)
                        balls.push_back(Sensor(atoi(balls_list)));
                }
                halftimes.push_back(Halftime(name_half, 1, 10, balls));
                balls.clear();
                fgets(row, 79, fp_meta);
            }

        } else if (strncmp(row, "Team A", 6) == 0) {
            team_a = Team("A", teamAnalysis(fp_meta, &sensor_players, &sensor_players_map));
        } else if (strncmp(row, "Team B", 6) == 0) {
            team_b = Team("B", teamAnalysis(fp_meta, &sensor_players, &sensor_players_map));
        }

        fgets(row, 79, fp_meta);
    }

    for (int i = 0; i < halftimes.size(); i++) {
        halftimes.at(i).printState();
    }

    sensor_players_map_private = sensor_players_map; //copia della map

    for (int i = 0; i < sensor_players.size(); i++) {
        sensor_players.at(i).printState();
    }

    //APERTURA FILE FULL-GAME

    FILE *fp_game;
    int current_active_ball_id;
    if ((fp_game = fopen("res/full-game", "r")) == NULL) {
        cout << "full game file not found";
        return 5;
    }

    FILE *fp_out, *fp_out_1, *fp_out_2, *fp_out_3, *fp_out_0;

    if ((fp_out = fopen("res/out.txt", "a+")) == NULL) {
        cout << "out file not found";
        return 5;
    }
    if ((fp_out_0 = fopen("res/out_0.txt", "w+")) == NULL || (fp_out_1 = fopen("res/out_1.txt", "w+")) == NULL || (fp_out_2 = fopen("res/out_2.txt", "w+")) == NULL || (fp_out_3 = fopen("res/out_3.txt", "w+")) == NULL) {
        cout << "out file not found";
        return 5;
    }
    vector<Data> data;
    Data active_ball;
    vector<Data> balls_substitutions; //evento in cui la palla è diversa dalla precedente attiva
    Halftime halftime1 = halftimes.at(0);
    vector<Interruption> interruptions; //eventi di interruzioni di gioco, separati begin e end
    int index_interruption = 0;

    readGameInterruptions(&interruptions);

    printf("Size interruptions: %ld\n", interruptions.size());

    /*** STAMPA A VIDEO DELLE INTERRUZIONI LETTE
    Interruption* interr;
    for(auto &interr: interruptions){
            interr.printState();
    }*/

    char row_game[200];
    fgets(row_game, 199, fp_game);
    int fine = 0;
    int sid, x, y, z;
    double timestamp;
    sid = atoi(strtok(row_game, ","));
    timestamp = atof(strtok(NULL, ","));
    x = atoi(strtok(NULL, ","));
    y = atoi(strtok(NULL, ","));
    z = atoi(strtok(NULL, ","));
    data.push_back(Data(sid, timestamp, x, y, z));
   /* if(halftimes.at(0).hasBall(sid)){ //se la palla letta è diversa dalla corrente, salva il puntatore al nuovo dato inserito.
        balls_substitutions.push_back(data.back());
        current_active_ball_id = sid;
    }*/
    double i_time, temp;
    i_time = 10753295594424116;
    cout << i_time << endl;
    int j = 0;
    bool flag_interrupted = false;

    while (fine == 0) {
        fgets(row_game, 199, fp_game);
        sid = atoi(strtok(row_game, ","));
        timestamp = atof(strtok(NULL, ","));
        x = atoi(strtok(NULL, ","));
        y = atoi(strtok(NULL, ","));
        z = atoi(strtok(NULL, ","));

        if(timestamp >= i_time) {
            Data d = Data(sid, timestamp, x, y, z);
            if (!isDataGameInterrupted(&d, &index_interruption, &interruptions)) {
                flag_interrupted = false;
                data.push_back(d);
            } else if (!flag_interrupted) { //aggiunge un evento di interruzione
                d.setId(-1);
                d.setTimestamp(interruptions.at(index_interruption).getTimestamp());
                data.push_back(d);
                flag_interrupted = true;
            }
             //check delle palle (assegnamento della palla)
            if (timestamp == i_time && halftime1.hasBall(sid)) {
                active_ball = Data(sid, timestamp, x, y, z);
                balls_substitutions.push_back(active_ball);
            }else {
                if(halftime1.hasBall(sid)){
                    if(sid!=balls_substitutions.at(balls_substitutions.size()-1).getId())
                        if(field.isInternal(x,y))
                            balls_substitutions.push_back(Data(sid, timestamp, x, y, z));
                }
            }
        }

        /*if(current_active_ball_id != sid && halftimes.at(0).hasBall(sid)){ //se la palla letta è diversa dalla corrente, salva il puntatore al nuovo dato inserito. PER TUTTI I TEMPI DA FARE
            balls_substitutions.push_back(data.back());
            current_active_ball_id = sid;
        }*/
        temp = timestamp;

        if (temp - i_time > 30 * pow(10, 12)) {
            fine = 1;
        }
    }

    cout << " fine file full_game" << endl;

    int chunkSize = data.size() / omp_get_max_threads();
    int TID;
    Data old_nearest=active_ball;
    double old_timestamp=active_ball.getTimestamp();
    double min = minimum_distance_mm;
    double distance=0;
    double time_inc;
    double now=0;
    int index_active_ball=0;

    vector<double> *vettore;

    //CODICE PER LA SOSTITUZIONE DEI PALLONI
    /*if(d.getTimestamp()>balls_substitutions.at(index_active_ball).getTimestamp()){
            for(int h=index_active_ball;h<balls_substitutions.size();h++) {
                if(d.getTimestamp()<=balls_substitutions.at(h).getTimestamp()) {
                    index_active_ball=h-1;
                    h=balls_substitutions.size();
                }
            }
            active_ball=balls_substitutions.at(index_active_ball);
            min = k*1000;
        }
        else */

    double wtime = omp_get_wtime ( ); //prende il tempo di inizio esecuzione del programma

#pragma omp parallel \
    firstprivate(TID, time_inc, active_ball, sensor_players_map_private, old_nearest, old_timestamp, min, distance, index_active_ball)
    {
#pragma omp for
    for (int i = 0; i < data.size(); i++) {
        Data d = data.at(i);


            if (d.getId() == active_ball.getId()) {
                active_ball = d;
                now = (d.getTimestamp() - i_time) / pow(10, 12);
                Data new_nearest = nearestData(positions, active_ball, minimum_distance_mm, old_nearest);
                distance = active_ball.distance(new_nearest);
                if (new_nearest.getId() != old_nearest.getId() && distance < minimum_distance_mm) {
                    if (old_nearest.getId() != active_ball.getId() && !halftime1.hasBall(old_nearest.getId())) {
                        time_inc = (d.getTimestamp() - old_timestamp) / pow(10, 12);
                        sensor_players_map_private[old_nearest.getId()].addFractionElement(time_inc);
                        fprintf(fp_out_0, "ball:   %d --> %lf   %lf\n", old_nearest.getId(), time_inc, now);
                        old_timestamp = d.getTimestamp();
                        old_nearest = new_nearest;
                        min = distance;
                    }
                } else if (distance > minimum_distance_mm) {
                    if (old_nearest.getId() != active_ball.getId() && !halftime1.hasBall(old_nearest.getId())) {
                        time_inc = (d.getTimestamp() - old_timestamp) / pow(10, 12);
                        sensor_players_map_private[old_nearest.getId()].addFractionElement(time_inc);
                        fprintf(fp_out_0, "ball:   %d --> %lf   %lf\n", old_nearest.getId(), time_inc, now);
                        old_timestamp = d.getTimestamp();
                        old_nearest = d;
                        min = minimum_distance_mm;
                    }
                }
            } else if (!halftime1.hasBall(d.getId()) && !d.isReferee() && d.getId() != -1) {
                positions[d.getId()] = d;
                distance = active_ball.distance(d);
                if (distance < min) {
                    min = distance;
                    //if(old_nearest.getId()!=d.getId()){
                    now = (d.getTimestamp() - i_time) / pow(10, 12);
                    if (old_nearest.getId() != active_ball.getId() && !halftime1.hasBall(old_nearest.getId())) {
                        time_inc = (d.getTimestamp() - old_timestamp) / pow(10, 12);
                        sensor_players_map_private[old_nearest.getId()].addFractionElement(time_inc);

                        fprintf(fp_out_0, "player: %d --> %lf   %lf\n", old_nearest.getId(), time_inc, now);
                    }
                    old_nearest = d;
                    old_timestamp = d.getTimestamp();
                    //}

                }
            } else if (d.getId() == -1){
                min = minimum_distance_mm;
                //if(old_nearest.getId()!=d.getId()){
                now = (d.getTimestamp() - i_time) / pow(10, 12);
                if (old_nearest.getId() != active_ball.getId() && !halftime1.hasBall(old_nearest.getId())) {
                    time_inc = (d.getTimestamp() - old_timestamp) / pow(10, 12);
                    sensor_players_map_private[old_nearest.getId()].addFractionElement(time_inc);

                    fprintf(fp_out_0, "player: %d --> %lf   %lf\n", old_nearest.getId(), time_inc, now);
                }
                old_nearest = active_ball;
                old_timestamp = data.at(i+1).getTimestamp();
            }
        }

            #pragma omp barrier

            #pragma omp critical
        {
            int somma_num = 0;

            for(auto &t: sensor_players_map_private){
                somma_num += t.second.getFractionVector()->size();
            }
            printf("[%d/%d] Size map privata: %d\n", omp_get_thread_num(), omp_get_num_threads(), somma_num);
            // concatena i vettori
            for (auto &t : sensor_players_map_private) {
                sensor_players_map[t.first].addFractionVector(sensor_players_map_private[t.first].getFractionVector());
            }

            vettore = sensor_players_map_private[38].getFractionVector();
            if(vettore->size()>0)
                printf("[%d/%d] Vettore %f\n", omp_get_thread_num(), omp_get_num_threads(), vettore->at(k));
            else
                printf("[%d/%d] Vettore vuoto\n", omp_get_thread_num(), omp_get_num_threads());

        }
    }

    double sum = 0;
    pair<int,SensorPlayer> tt;
    SensorPlayer sp;
    double collection;
//#pragma omp parallel for private (sum, sp)
    for (auto &tt: sensor_players_map) {

        sp = tt.second;
        collection = sp.collectFractions();
        sp.getPlayer()->addPossession(collection);

        //  printf("Possesso aggiunto [%s][%d]: %f\n", sp.getPlayer()->getName().c_str(),sp.getId(), sum);



    }




    /*#pragma omp sections nowait
    {
#pragma omp section
        {
            team_a.collectPossession();
        }
#pragma omp section
        {
            team_b.collectPossession();
        }
    }*/
    team_a.collectPossession();
    team_b.collectPossession();
    /*
    * Misura il tempo di esecuzione da prima del primo pragma fino a qui
    */
    wtime = omp_get_wtime ( ) - wtime; // <-- Stop the clock
    printf ( "\nExecution took %g seconds.\n\n", wtime );

    team_a.printState();
    team_b.printState();
    fclose(fp_out);
    return 0;
}

Data nearestData(map<int, Data> map, Data ball,double min,Data old_nearest) {
    Data datamin=old_nearest;
    double distance;
    for (auto &d: map) {
        distance=d.second.distance(ball);
        if(distance<=min) {
            datamin = d.second;
            min=distance;
        }
    }
    return datamin;
}

vector<Player*> teamAnalysis(FILE* fp_meta,vector<SensorPlayer> * sensor_players, map<int,SensorPlayer> * sensor_players_map){
    vector<Player*>  players;
    char row[80];
    fgets(row, 79, fp_meta);
    fgets(row, 79, fp_meta);
    fgets(row, 79, fp_meta);

    int j = 0;
    char* token;
    Player * player;
    SensorPlayer sensor_p;
    while (strncmp(row,"\r",1) != 0){

        token = strtok (row, "(");
        player=new Player(token);
        players.push_back(player);

        int token_sensor;
        while (token != NULL){
            token = strtok(NULL, ":,");
            if(token != NULL){
                token_sensor = atoi(token);
                if(token_sensor != 0){
                    sensor_p=SensorPlayer(token_sensor,player);
                    sensor_players->push_back(sensor_p);
                    (*sensor_players_map)[token_sensor] = sensor_p;
                }

            }
        }
        fgets(row, 79, fp_meta);
    }

    return players;
}

void readGameInterruptions(vector<Interruption> * interruptions){
    FILE* fp_int;

    io::CSVReader<2, io::trim_chars<>, io::no_quote_escape<';'> > in("res/interruptions.csv");
    in.read_header(io::ignore_extra_column, "event_id", "timestamp");
    int event_id; double timestamp;
    while(in.read_row(event_id, timestamp)){
        // printf("Riga letta: ET %s TS %f\n", event_id == 2010 ? "Int Begin" : "Int End", timestamp);
        if(event_id == 2010) { //id BEGIN
            interruptions->push_back(Interruption(timestamp, interr_begin));
        }
        else if(event_id == 2011){ //id END
            interruptions->push_back(Interruption(timestamp, interr_end));
        }

    }

}

/***
 * Controlla se il dato letto è successivo all'interruzione in considerazione e in caso affermativo la aggiorna con quella seguente.
 * @param d
 * @param index_interruption
 * @param interruptions
 * @return True se il gioco è interrotto, False se il gioco è in svolgimento
 */
bool isDataGameInterrupted(Data * d, int * index_interruption, vector<Interruption> * interruptions){
    //se il timestamp del dato letto è maggiore della interruption, essendo in ordine temporale, significa che bisogna aggiornare l'indice delle interruptions, perché è cambiato lo stato del gioco
    if(*index_interruption<interruptions->size()-1 && d->getTimestamp() >= interruptions->at(1+ *index_interruption).getTimestamp()){
        (*index_interruption)++;
    }
    //si può quindi procedere all'analisi
    if(d->getTimestamp() >= interruptions->at(*index_interruption).getTimestamp()) {
        TypeOfEvent current_type = (interruptions->at(*index_interruption)).getType();
        if (current_type == interr_begin) {
            //printf("I%d, ", *index_interruption);
            return true;
        } else if (current_type == interr_end) {
            //printf("I%d, ", *index_interruption);
            return false;
        }
    }
    return false;
}




