//
// Created by Daniele on 13/08/2018.
//

#ifndef SOCCER_SENSORPLAYER_H
#define SOCCER_SENSORPLAYER_H


#include "Sensor.h"
#include "Player.h"
#include <vector>

class SensorPlayer : public Sensor {
public:
    SensorPlayer();
    SensorPlayer(int id_i,Player * player_i);
    Player * getPlayer();
    void addFractionVector(vector<double> * fractions);
    vector <double> * getFractionVector();
    void addFractionElement(double fraction);
    double collectFractions();

    void printState();
private:
    Player * player;
    vector<double> fraction_possession_time;
};


#endif //SOCCER_SENSORPLAYER_H
