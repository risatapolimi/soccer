//
// Created by Daniele on 14/08/2018.
//

#include "SoccerField.h"

SoccerField::SoccerField(int xl,int xr,int yu,int yd){
   x_left=xl;
   x_right=xr;
   y_up=yu;
   y_down=yd;
}

int SoccerField::getX_left() const {
    return x_left;
}

int SoccerField::getX_right() const {
    return x_right;
}

int SoccerField::getY_up() const {
    return y_up;
}

int SoccerField::getY_down() const {
    return y_down;
}

bool SoccerField::isInternal(int x,int y){
    return x>x_left && x<x_right && y<y_up && y>y_down;
}
