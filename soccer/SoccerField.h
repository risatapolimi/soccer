//
// Created by Daniele on 14/08/2018.
//

#ifndef SOCCER_SOCCERFIELD_H
#define SOCCER_SOCCERFIELD_H


class SoccerField {
public:
    SoccerField(int xl,int xr,int yu,int yd);
    int getX_left() const;
    int getX_right() const;
    int getY_up() const;
    int getY_down() const;
    bool isInternal(int x,int y);

private:
// (0,33965), (0,-33960),(52483,33965),(52483,-33960)
    int x_left,x_right,y_up,y_down;

};


#endif //SOCCER_SOCCERFIELD_H
