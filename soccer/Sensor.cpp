//
// Created by Daniele on 13/08/2018.
//

#include "Sensor.h"

Sensor::Sensor() {
    id=0;
}

Sensor::Sensor(int id_i){
    id=id_i;
}

int Sensor::getId(){
    return id;
}

void Sensor::printState() {
    cout << "SensorID: " << id << " \t";
}
