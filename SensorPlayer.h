#include "Player.h"

using namespace std;

class SensorPlayer : public Sensor{
	Player p;

public:
	SensorPlayer (int id, Player player) : Sensor(id,100){
		p = player;
	}
};